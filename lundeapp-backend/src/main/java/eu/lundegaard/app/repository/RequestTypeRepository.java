package eu.lundegaard.app.repository;

import eu.lundegaard.app.entity.RequestType;
import org.springframework.data.repository.CrudRepository;

public interface RequestTypeRepository extends CrudRepository<RequestType, Long> {
}

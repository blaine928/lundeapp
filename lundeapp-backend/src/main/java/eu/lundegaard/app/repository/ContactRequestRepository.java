package eu.lundegaard.app.repository;

import eu.lundegaard.app.entity.ContactRequest;
import org.springframework.data.repository.CrudRepository;

public interface ContactRequestRepository extends CrudRepository<ContactRequest, Long> {
}

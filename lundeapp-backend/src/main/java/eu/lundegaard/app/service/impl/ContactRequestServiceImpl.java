package eu.lundegaard.app.service.impl;

import eu.lundegaard.app.entity.ContactRequest;
import eu.lundegaard.app.repository.ContactRequestRepository;
import eu.lundegaard.app.service.ContactRequestService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContactRequestServiceImpl implements ContactRequestService {

    private final ContactRequestRepository repository;

    public ContactRequestServiceImpl(ContactRequestRepository repository) {
        this.repository = repository;
    }

    @Override
    public Long storeContactRequest(ContactRequest contactRequest) {
        return repository.save(contactRequest).getId();
    }

    @Override
    public Optional<ContactRequest> findContactRequestById(Long id) {
        return repository.findById(id);
    }


}

package eu.lundegaard.app.service.impl;

import com.google.common.collect.Lists;
import eu.lundegaard.app.entity.RequestType;
import eu.lundegaard.app.repository.RequestTypeRepository;
import eu.lundegaard.app.service.RequestTypeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestTypeServiceImpl implements RequestTypeService {

    private final RequestTypeRepository requestTypeRepository;

    public RequestTypeServiceImpl(RequestTypeRepository requestTypeRepository) {
        this.requestTypeRepository = requestTypeRepository;
    }

    @Override
    public List<RequestType> getAllRequestTypes() {
        return Lists.newArrayList(requestTypeRepository.findAll());
    }
}

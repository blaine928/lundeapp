package eu.lundegaard.app.service;

import eu.lundegaard.app.entity.ContactRequest;

import java.util.Optional;

public interface ContactRequestService {

    Long storeContactRequest(ContactRequest contactRequest);

    Optional<ContactRequest> findContactRequestById(Long id);

}

package eu.lundegaard.app.service;

import eu.lundegaard.app.entity.RequestType;

import java.util.List;

public interface RequestTypeService {

    List<RequestType> getAllRequestTypes();

}

package eu.lundegaard.app.resource;

import eu.lundegaard.app.dto.ContactRequestDto;
import eu.lundegaard.app.mapper.ContactRequestMapper;
import eu.lundegaard.app.service.ContactRequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

@RestController
@Validated
public class ContactRequestResource {

    static final String API_PATH = "/api/contact-request";

    private final ContactRequestService service;

    private final ContactRequestMapper mapper;

    public ContactRequestResource(ContactRequestService service,
                                  ContactRequestMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping(value = API_PATH, consumes = "application/json")
    public ResponseEntity<Void> saveContactRequest(@RequestBody @Valid ContactRequestDto request) {
        Long createdId = service.storeContactRequest(mapper.dtoToEntity(request));
        return ResponseEntity.created(URI.create(API_PATH + "/" + createdId)).build();
    }

    @GetMapping(value = API_PATH + "/{contactRequestId}")
    public ResponseEntity<ContactRequestDto> getById(@PathVariable("contactRequestId") Long contactRequestId) {
        return ResponseEntity.of(
                service.findContactRequestById(contactRequestId)
                        .map(mapper::entityToDto)
        );
    }

}

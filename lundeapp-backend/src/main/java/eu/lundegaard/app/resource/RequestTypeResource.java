package eu.lundegaard.app.resource;

import eu.lundegaard.app.dto.RequestTypeDto;
import eu.lundegaard.app.mapper.RequestTypeMapper;
import eu.lundegaard.app.service.RequestTypeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class RequestTypeResource {

    private final RequestTypeService service;

    private final RequestTypeMapper mapper;

    public RequestTypeResource(RequestTypeService service,
                               RequestTypeMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping(path = "/api/request-types")
    public ResponseEntity<List<RequestTypeDto>> getAllRequestTypes() {
        return ResponseEntity.ok(
                service.getAllRequestTypes().stream()
                        .map(mapper::entityToDto)
                        .collect(Collectors.toList())
        );
    }

}

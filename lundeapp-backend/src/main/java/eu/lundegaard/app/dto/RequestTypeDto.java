package eu.lundegaard.app.dto;

import lombok.Data;

@Data
public class RequestTypeDto {

    private Long id;
    private String name;

}

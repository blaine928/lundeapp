package eu.lundegaard.app.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class ContactRequestDto {

    @NotNull(message = "Request Type must be defined")
    private Long requestTypeId;

    @Pattern(regexp = "[a-zA-Z0-9]+", message = "Policy Number must not be empty and can only contain alphanumeric characters")
    @Size(max = 255, message = "Maximum length for Policy Number is 255 characters")
    private String policyNumber;

    @Pattern(regexp = "[a-zA-Z]+", message = "Name must not be empty and can only contain letters")
    @Size(max = 255, message = "Maximum length for Name is 255 characters")
    private String name;

    @Pattern(regexp = "[a-zA-Z]+", message = "Surname must not be empty and can only contain letters")
    @Size(max = 255, message = "Maximum length for Surname is 255 characters")
    private String surname;

    @Size(min = 1, max = 5000, message = "Request text must not be empty and maximum size is 5000 characters")
    private String requestText;

}

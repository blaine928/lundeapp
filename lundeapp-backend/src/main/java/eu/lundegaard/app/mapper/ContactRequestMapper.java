package eu.lundegaard.app.mapper;

import eu.lundegaard.app.dto.ContactRequestDto;
import eu.lundegaard.app.entity.ContactRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = RequestTypeMapper.class)
public interface ContactRequestMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "requestTypeId", target = "requestType")
    ContactRequest dtoToEntity(ContactRequestDto contactRequestDto);

    @Mapping(source = "requestType.id", target = "requestTypeId")
    ContactRequestDto entityToDto(ContactRequest contactRequest);

}

package eu.lundegaard.app.mapper;

import eu.lundegaard.app.dto.RequestTypeDto;
import eu.lundegaard.app.entity.RequestType;
import eu.lundegaard.app.repository.RequestTypeRepository;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class RequestTypeMapper {

    @Autowired
    private RequestTypeRepository requestTypeRepository;

    public abstract RequestTypeDto entityToDto(RequestType requestType);

    RequestType idToEntity(Long requestTypeId) {
        return requestTypeRepository
                .findById(requestTypeId)
                .orElseThrow(() -> new IllegalArgumentException("Request type " + requestTypeId + " does not exist"));
    }

}

package eu.lundegaard.app.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class ContactRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private RequestType requestType;

    private String policyNumber;

    private String name;

    private String surname;

    private String requestText;

}

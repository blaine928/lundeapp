package eu.lundegaard.app.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class RequestType {

    @Id
    private Long id;

    private String name;

}

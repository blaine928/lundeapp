package eu.lundegaard.app.resource;

import eu.lundegaard.app.dto.RequestTypeDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RequestTypeResourceTest {

    @Autowired
    private RequestTypeResource resource;

    @Test
    public void testLoadAllTypes() {
        List<RequestTypeDto> requestTypes = resource.getAllRequestTypes().getBody();
        assertNotNull(requestTypes);
        assertTrue(requestTypes.size() > 0);
    }

}
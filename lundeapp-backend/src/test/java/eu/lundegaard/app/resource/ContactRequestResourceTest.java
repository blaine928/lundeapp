package eu.lundegaard.app.resource;

import eu.lundegaard.app.dto.ContactRequestDto;
import eu.lundegaard.app.entity.RequestType;
import eu.lundegaard.app.repository.RequestTypeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContactRequestResourceTest {

    private static final long REQUEST_TYPE_ID = -9999L;
    private static final long INVALID_REQUEST_TYPE_ID = -8888L;

    @Autowired
    private ContactRequestResource resource;

    @Autowired
    private RequestTypeRepository requestTypeRepository;

    @Before
    public void setUp() throws Exception {
        RequestType requestType = new RequestType();
        requestType.setId(REQUEST_TYPE_ID);
        requestType.setName("Test");
        requestTypeRepository.save(requestType);
    }

    @Test
    public void testSaveRequestSuccess() {
        ContactRequestDto requestDto = createRequestDto();
        ResponseEntity<Void> responseEntity = resource.saveContactRequest(requestDto);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        List<String> locationHeaders = responseEntity.getHeaders().get(HttpHeaders.LOCATION);
        assertNotNull(locationHeaders);
        assertEquals(1, locationHeaders.size());
        String header = locationHeaders.get(0);
        assertTrue(header.contains(ContactRequestResource.API_PATH));
        String idString = header.replace(ContactRequestResource.API_PATH + "/", "");
        assertNotNull(idString);
        long id = Long.parseLong(idString);
        ContactRequestDto createdDto = resource.getById(id).getBody();
        assertNotNull(createdDto);
        assertEquals(requestDto, createdDto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSaveRequestInvalidType() {
        ContactRequestDto requestDto = createRequestDto();
        requestDto.setRequestTypeId(INVALID_REQUEST_TYPE_ID);
        resource.saveContactRequest(requestDto);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testSaveRequestNullName() {
        ContactRequestDto requestDto = createRequestDto();
        requestDto.setName(null);
        resource.saveContactRequest(requestDto);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testSaveRequestNullSurname() {
        ContactRequestDto requestDto = createRequestDto();
        requestDto.setSurname(null);
        resource.saveContactRequest(requestDto);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testSaveRequestNullPolicyNumber() {
        ContactRequestDto requestDto = createRequestDto();
        requestDto.setPolicyNumber(null);
        resource.saveContactRequest(requestDto);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testSaveRequestNullRequestText() {
        ContactRequestDto requestDto = createRequestDto();
        requestDto.setRequestText(null);
        resource.saveContactRequest(requestDto);
    }

    private ContactRequestDto createRequestDto() {
        ContactRequestDto request = new ContactRequestDto();
        request.setRequestTypeId(REQUEST_TYPE_ID);
        request.setName("ValidName");
        request.setSurname("ValidSurname");
        request.setPolicyNumber("ValidPolicyNumber123");
        request.setRequestText("Valid request text 123 ##!@");
        return request;
    }


}
import React from 'react';
import Button from '@material-ui/core/Button';
import {makeStyles} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from "@material-ui/core/Typography";


const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        maxWidth: 500,
    },
    header: {
        marginTop: 100,
        marginBottom: 50,
    },
    results: {
        width: 400,
        paddingTop: 10,
        margin: 0,
    },
    item: {
        width: 400,
    },
    textField: {
        margin: theme.spacing(1),
        width: 300,
    },
    textarea: {
        margin: theme.spacing(1),
        width: 300,
    },
    formControl: {
        margin: theme.spacing(1),
        width: 300,
    },
}));

export default function MainScreen() {

    const API_PATH = 'http://localhost:8080/api';

    const classes = useStyles();
    const [values, setValues] = React.useState({
        type: '',
        policyNumber: '',
        name: '',
        surname: '',
        requestText: '',
    });
    const [types, setTypes] = React.useState([
        {
            id: 0,
            name: 'None'
        },
    ]);
    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    const [results, setResults] = React.useState([]);

    function loadRequestTypes() {
        fetch(API_PATH + '/request-types')
            .then(response => response.json())
            .then(responseJson => setTypes(responseJson))
            .catch(e => console.log(e));
    }

    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
        loadRequestTypes();
    }, []);

    const handleChange = name => event => {
        setValues({...values, [name]: event.target.value});
    };

    function handleSubmit() {
        setResults([]);
        fetch(API_PATH + '/contact-request', {
            method: 'post',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                requestTypeId: values.type,
                policyNumber: values.policyNumber,
                name: values.name,
                surname: values.surname,
                requestText: values.requestText,
            })
        }).then(res => {
            if (!res.ok) {
                res.json()
                    .then(res => {
                        if (res.errors && res.errors.length > 0) {
                            let errors = res.errors.map(res => {
                                return ({
                                    error: true,
                                    message: res.defaultMessage
                                });
                            });
                            setResults(errors);
                        } else {
                            setResults([{
                                error: true,
                                message: res.error
                            }]);
                        }
                    });
            } else {
                setResults([{
                    error: false,
                    message: 'Request was successfully sent'
                }]);
            }
        });

    }

    return (
        <Container className={classes.root}>
            <Grid container>
                <Grid item className={classes.item}>
                    <Typography variant="h2" className={classes.header} gutterBottom>
                        Lunde App
                    </Typography>
                </Grid>
                <Grid item className={classes.item}>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel ref={inputLabel} htmlFor="request-type">
                            Kind of Request
                        </InputLabel>
                        <Select
                            value={values.type}
                            onChange={handleChange('type')}
                            labelWidth={labelWidth}
                        >
                            {types.map(type => {
                                return (<MenuItem key={type.id} value={type.id}>{type.name}</MenuItem>);
                            })}
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item className={classes.item}>
                    <TextField
                        id="policy-number"
                        label="Policy Number"
                        className={classes.textField}
                        value={values.policyNumber}
                        onChange={handleChange('policyNumber')}
                        margin="normal"
                        variant="outlined"
                    />
                </Grid>
                <Grid item className={classes.item}>
                    <TextField
                        id="name"
                        label="Name"
                        className={classes.textField}
                        value={values.name}
                        onChange={handleChange('name')}
                        margin="normal"
                        variant="outlined"
                    />
                </Grid>
                <Grid item className={classes.item}>
                    <TextField
                        id="surname"
                        label="Surname"
                        className={classes.textField}
                        value={values.surname}
                        onChange={handleChange('surname')}
                        margin="normal"
                        variant="outlined"
                    />
                </Grid>
                <Grid item className={classes.item}>
                    <TextField
                        id="request-text"
                        label="Your Request"
                        multiline
                        className={classes.textarea}
                        value={values.requestText}
                        onChange={handleChange('requestText')}
                        margin="normal"
                        variant="outlined"
                        rows={5}

                    />
                </Grid>
                <Grid item className={classes.item}>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={handleSubmit}
                    >
                        Send Request
                    </Button>
                </Grid>
                <Grid item className={classes.item}>
                    {results.map(result => {
                        return (
                            <Typography
                                key={result.message}
                                variant="inherit"
                                className={classes.results}
                                color={result.error ? "error" : "inherit"}
                                paragraph={true}
                            >
                                {result.message}
                            </Typography>
                        );
                    })}
                </Grid>
            </Grid>
        </Container>
    )

}
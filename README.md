# Lunde App

This application was made for the purpose of code review in the company Lundegaard.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Have Java version 8+ installed.

(Linux only) Have permissions to execute the gradle wrapper script

```
chmod +x ./gradlew 
```

### Build

A single command is required to run the build.

Windows:
```
gradlew.bat clean build
```

Linux:

```
./gradlew clean build
```


## Running the app

After build, the application can be run with a single command. 

Windows:
```
java -jar lundeapp-backend\build\libs\lundeapp-backend-0.0.1-SNAPSHOT.jar
```

Linux:
``` 
java -jar lundeapp-backend/build/libs/lundeapp-backend-0.0.1-SNAPSHOT.jar
```

Please note that once you change the version, the runnable JAR file will have a different name :-)

Open the Frontend in browser:
``` 
localhost:8080
```

Frontend application is included in the backend JAR and is run automatically together. 
If you want to run Frontend separately for any reason, you can do it with npm (node.js is required for that).
It will open a browser window with URL localhost:3000 and connect to the same backend running on port 8080.

``` 
cd lundeapp-frontend
npm start
```

## Acknowledgements

Special thanks to:

* Google

* Stackoverflow

* Baeldung